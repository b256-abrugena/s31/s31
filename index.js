const http = require("http");

const port = 3000;

const server = http.createServer(function(request, response){

	if(request.url == "/login"){
		response.writeHead(200, {'Content-type': 'text/plain'});

		response.end("You are in the log-in page.");
	}

	else{
		response.writeHead(404, {'Content-type': 'text/plain'});
		
		response.end("Error");
	}

});

server.listen(port);

console.log(`Server had successfully running at localhost:${port}`);